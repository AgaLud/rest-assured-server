package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class PutComments {

    final String COMMENTS_URL = "http://localhost:3000/comments";

    @Test
    void shouldReturnCode201WhenCommentIsCreated() {
        Map<String, Object> pullBody = new HashMap<>();
        pullBody.put("body", "NEW2 comment");

        given().pathParam("id", 2)
                .contentType(ContentType.JSON)
                .and().body(pullBody)
                .when().put(COMMENTS_URL + "/{id}")
                .then().statusCode(200).and().body("body", is("NEW2 comment"));
    }
}
