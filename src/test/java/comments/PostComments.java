package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class PostComments {

    final String COMMENTS_URL = "http://localhost:3000/comments";

    @Test
    void shouldReturnCode201WhenCommentIsCreated() {
        Map<String, Object> postBody = new HashMap<>();
        postBody.put("body", "some comment");
        postBody.put("postId", 1);

        given().contentType(ContentType.JSON)
                .and().body(postBody).
                when().post(COMMENTS_URL).
                then().statusCode(201);
    }
}
