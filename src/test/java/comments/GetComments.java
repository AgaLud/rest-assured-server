package comments;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

public class GetComments {

    final String COMMENTS_URL = "http://localhost:3000/comments";

    @Test
    void shouldReturnStatusCode200whenGettingAllComments() {
        when().get(COMMENTS_URL).
                then().statusCode(200);
    }

    @Test
    void shouldReturnCorrectBodyStructureWhenGettingSingleComment() {
        given().pathParam("id", 2)
                .when().get(COMMENTS_URL + "/{id}")
                .then().body("id", is(2))
                .and().body("postId", is(1))
                .and().body("body", containsString("some comment"));
        ;
    }
}
